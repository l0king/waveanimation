import 'dart:ui';
import 'dart:math';

import 'package:flutter/material.dart';

import '../utils/random_color_generator.dart';
import '../widgets/animated/wave_from_tap.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen>
    with SingleTickerProviderStateMixin, RandomColorGenerator {
  AnimationController _waveController;
  Animation<double> _waveAnimation;
  var _tapPoint = Offset(1.0, 1.0);
  var _backgroundColor = Colors.black;
  var _waveColor = Colors.black;

  void _changeColor(context) {
    if (_waveController.isCompleted) {
      setState(() {
        _backgroundColor = _waveColor;
        _waveColor = randomColor;
      });
      _waveController.reset();
    }
    _waveController.forward();
  }

  _setTapPoint(TapDownDetails details) {
    _tapPoint = details.globalPosition;
  }

  double _calcDiagonal(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final diagonal = sqrt(pow(size.height, 2) + pow(size.width, 2));
    return diagonal;
  }

  @override
  void initState() {
    _waveController = AnimationController(
      duration: Duration(milliseconds: 500),
      vsync: this,
    );
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_waveAnimation == null) {
      _waveAnimation = Tween<double>(
        begin: 0.0,
        end: _calcDiagonal(context),
      ).animate(_waveController);
    }
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _waveController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (details) {
        _changeColor(context);
        _setTapPoint(details);
      },
      child: Stack(
        children: <Widget>[
          Container(
            color: _backgroundColor,
          ),
          Container(
            child: WaveFromTap(
              animation: _waveAnimation,
              tapPoint: _tapPoint,
              color: _waveColor,
            ),
          ),
          Center(
            child: const Text(
              'Hey there',
              style: TextStyle(
                decoration: TextDecoration.none,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
